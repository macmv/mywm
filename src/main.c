#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <assert.h>

#include <wayland-server.h>
#include <wlr/backend.h>

#include "server.h"
#include "drm.h"

struct wm_server server;

void on_signal(int sig) {
  printf("\nClosing...\n");
  wl_display_destroy(server.display);
  exit(0);
}

int main() {
  server.display = wl_display_create();
  assert(server.display);
  server.event_loop = wl_display_get_event_loop(server.display);
  assert(server.event_loop);

  server.backend = wlr_backend_autocreate(server.display, NULL);
  assert(server.backend);

  server.socket = wl_display_add_socket_auto(server.display);
  assert(server.socket);
  setenv("WAYLAND_DISPLAY", server.socket, 1);

  signal(SIGINT, on_signal);

  init();

  if (!wlr_backend_start(server.backend)) {
    fprintf(stderr, "Failed to start backend\n");
    wl_display_destroy(server.display);
    return 1;
  }

  printf("Running display on socket %s\n", server.socket);
  wl_display_run(server.display);
  wl_display_destroy(server.display);
}
