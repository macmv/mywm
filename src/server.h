#pragma once

#include <stdint.h>

#include <wlr/backend.h>
#include <wayland-server.h>

extern struct wm_server {
  struct wl_display* display;
  struct wl_event_loop* event_loop;
  const char* socket;

  struct wlr_backend* backend;
  struct wlr_compositor *compositor;

  struct wl_listener on_new_output;

  // Linked list of outputs
  // There won't be that many outputs,
  // so this is most likely the fastest
  // way to do things
  struct wl_list outputs;
} server;

struct wm_output {
  struct wlr_output *wlr_output;
  struct timespec last_frame;
  float color[4];
  int dec;

  struct wl_listener on_destroy;
  struct wl_listener on_frame;

  // Linked list
  struct wl_list link;
};

void init();

