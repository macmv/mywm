#include "server.h"
#include "drm.h"

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <wayland-client.h>
#include <wayland-server.h>
#include <wlr/backend.h>
#include <wlr/render/wlr_renderer.h>
#include <wlr/types/wlr_output.h>

// Called when a new window should be opened
// IE, whenever a new client connects
void on_new_window(struct wl_client* client, void* data, uint32_t version, uint32_t id) {
  printf("On new window\n");
}

// Called when a monitor is disconnected/removed
void on_output_destroy(struct wl_listener *listener, void *data) {
  struct wm_output *output = wl_container_of(listener, output, on_destroy);
  wl_list_remove(&output->link);
  wl_list_remove(&output->on_destroy.link);
  wl_list_remove(&output->on_frame.link);
  free(output);
}

// Called whenever a monitor needs to render a frame
void on_output_frame(struct wl_listener *listener, void *data) {
  struct wm_output* output = wl_container_of(listener, output, on_frame);
  struct wlr_output* wlr_output = data;
  struct wlr_renderer* renderer = wlr_backend_get_renderer(wlr_output->backend);

  // Simple blue color. Easier on the eye than a full red or something.
  float color[4] = {0.40, 0.66, 1.00, 1.0};

  wlr_output_attach_render(wlr_output, NULL);

  wlr_renderer_begin(renderer, wlr_output->width, wlr_output->height);
  wlr_renderer_clear(renderer, color);
  wlr_renderer_end(renderer);

  wlr_output_commit(wlr_output);
}

// Called when a new physical monitor is plugged in
void on_new_output(struct wl_listener* listener, void* data) {
  struct wlr_output* wlr_output = data;

  if (!wl_list_empty(&wlr_output->modes)) {
    struct wlr_output_mode *mode = wl_container_of(wlr_output->modes.prev, mode, link);
    wlr_output_set_mode(wlr_output, mode);
  }

  struct wm_output *output = calloc(1, sizeof(struct wm_output));
  clock_gettime(CLOCK_MONOTONIC, &output->last_frame);
  output->wlr_output = wlr_output;
  wl_list_insert(&server.outputs, &output->link);

  output->on_destroy.notify = on_output_destroy;
  wl_signal_add(&wlr_output->events.destroy, &output->on_destroy);
  output->on_frame.notify = on_output_frame;
  wl_signal_add(&wlr_output->events.frame, &output->on_frame);
}

void init() {
  init_drm(&server);

  wl_list_init(&server.outputs);

  server.on_new_output.notify = on_new_output;
  wl_signal_add(&server.backend->events.new_output, &server.on_new_output);

  // wl_global_create(server.display, &wl_output_interface, 1, NULL, wl_output_handle_bind);
}
